//Create a modules array containing the json data for the modules
modules = [
    {
        "id": "CIS2169",
        "name": "Web Application Development",
        "hours": "48",
        "LearningOutcome1": "Understand How to Develop a basic HTML, CSS and JavaScript Application",
        "LearningOutcome2": "Understand project modelling methods",
        "NumberofCredits": "20",
        "Assessments": "A-01, A-02",
        "Program": "Web Design & Development",
        "Room": "THF01",
        "Time": "14:00",
    },
    {
        "id": "CIS2148",
        "name": "Fundamentals of UX Design",
        "hours": "48",
        "LearningOutcome1": "Understanding UX Design Principles",
        "LearningOutcome2": "Understand how to follow the UX Design Process",
        "NumberofCredits": "20",
        "Assessments": "AS-01",
        "Program": "Computing",
        "Room": "THF01",
        "Time": "10:00",
    },

    {
        "id": "CIS2152",
        "name": "Fundamentals of Web Coding",
        "hours": "48",
        "LearningOutcome1": "Understand core PHP code methods",
        "LearningOutcome2": "Understand core JavaScript methods",
        "NumberofCredits": "20",
        "Assessments": "A2",
        "Program": "Web Design & Development",
        "Room": "THG03",
        "Time": "09:00",
    },

    {
        "id": "GEO1034",
        "name": "Fundamentals of Geographical Scinces",
        "hours": "96",
        "LearningOutcome1": "Understand core PHP code methods",
        "LearningOutcome2": "Understand core JavaScript methods",
        "NumberofCredits": "20",
        "Assessments": "N/A",
        "Program": "Geoenvironmental Hazards",
        "Room": "GEOS03",
        "Time": "16:00",
    }
];

// Get the contentContainer div on the page and use the .innerHTML to convert what comes after 
//into html compatible text and opens a literal value to hold this
document.getElementById("contentContainer").innerHTML = ` 
${modules.map(function(module) { // Maps the module array one object at a time into the module pararmeter which returns the specified values
    return `
    <div id="content">
        <p>ModuleID | ${module.id}</p>
        <p>Module Name | ${module.name}</p>
        <p>Module Hours | ${module.hours}</p>
        <p>Learning Outcome 1 | ${module.LearningOutcome1}</p>
        <p>Learning Outcome 2 | ${module.LearningOutcome2}</p>
        <p>Number of Credits | ${module.NumberofCredits}</p>
        <p>Assessments | ${module.Assessments}</p>
        <p>Room | ${module.Room}</p>
        <p>Time | ${module.Time}</p>
    </div>
    `
}).join('')}
`
//Join function removes any commas, or joining characters from between array elements
