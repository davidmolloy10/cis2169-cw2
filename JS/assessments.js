//Create an Assessments arrary containing the data for the assessments
assessments = [
    {
        "id": "1",
        "Title": "CW1",
        "Number": "A-01",
        "LearningOutcomeCovered": "LOC1",
        "Volume": "3000 words",
        "Weighting": "20%",
        "Module": "CIS2169"
    },
    {
        "id": "2",
        "Title": "CW2",
        "Number": "A-02",
        "LearningOutcomeCovered": "LOC2",
        "Volume": "7000 words",
        "Weighting": "80%",
        "Module": "CIS2169"
    },

    {
        "id": "3",
        "Title": "CW1",
        "Number": "AS-01",
        "LearningOutcomeCovered": "LOC1",
        "Volume": "1500 words",
        "Weighting": "15%",
        "Module": "CIS2148"
    },

    {
        "id": "4",
        "Title": "CW2",
        "Number": "A2",
        "LearningOutcomeCovered": "LOC2",
        "Volume": "9000 words",
        "Weighting": "85%",
        "Module": "CIS2152"
    }
];

//Gets the contentConatiner div on the page and uses the .innerHTML to convert what comes after
//into HTML compatible text and opens a literal value to hold the information
document.getElementById("contentContainer").innerHTML = ` 
${assessments.map(function(assessment) { //Maps the module array one object at a time into the module pararmeter which returns the specified values
    return `
    <div id="content">
        <p>Assessment | ${assessment.Title}</p>
        <p>Assessment Number | ${assessment.Number}</p>
        <p>Learning Outcome Covered | ${assessment.LearningOutcomeCovered}</p>
        <p>Volume | ${assessment.Volume}</p>
        <p>Assessment Weighting | ${assessment.Weighting}</p>
        <p>Module | ${assessment.Module}</p>
    </div>
    `
}).join('')} 
`
//Join function removes any commas, or joining characters from between array elements

