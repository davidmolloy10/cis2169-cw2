//Creates an arrary called slots containing the json data for the timeslots
slots = [
    {
        "Room": "THF01",
        "Time": "14:00",
        "Type": "Regular"
    },

    {
        "Room": "THG03",
        "Time": "09:00",
        "Type": "Irregular"
    },

    {
        "Room": "THF01",
        "Time": "10:00",
        "Type": "Regular"
    },

    {
        "Room": "MBG06",
        "Time": "12:00",
        "Type": "Regular"
    },

    {
        "Room": "GEOS03",
        "Time": "14:00",
        "Type": "Irregular"
    },

    {
        "Room": "THF01",
        "Time": "16:00",
        "Type": "Regular"
    },
];

// Get the contentContainer div on the page and use the .innerHTML to convert what comes after 
//into html compatible text and opens a literal value to hold this
document.getElementById("contentContainer").innerHTML = `
${slots.map(function(slots) { // Maps the module array one object at a time into the module pararmeter which returns the specified values
    return `
    <div id="content">
        <p>Room | ${slots.Room}</p>
        <p>Time | ${slots.Time}</p>
        <p>Type |${slots.Type}</p>
    </div>
    `
}).join('')} 
`
//Join function removes any commas, or joining characters from between array elements


