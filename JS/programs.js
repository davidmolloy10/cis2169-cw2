// Creates a pograms array containing json data for the degree programs
programs = [
    {
        "id": "1",
        "Name": "Computing",
        "LearningOutcome": "Understand the core fundamentals of Computing",
        "ExitAward": "BSc",
    },

    {
        "id": "2",
        "Name": "History",
        "LearningOutcome": "Understanding world history",
        "ExitAward": "BA",
    },

    {
        "id": "3",
        "Name": "Early years Education",
        "LearningOutcome": "Understanding how to teach students effectively",
        "ExitAward": "BEd",
    },

    {
        "id": "4",
        "Name": "Business & Management",
        "LearningOutcome": "Understanding how to manage business entities",
        "ExitAward": "BSc",
    },

    {
        "id": "5",
        "Name": "Web Design & Development",
        "LearningOutcome": "Understanding core concepts of the web and websites",
        "ExitAward": "BSc",
    },

    {
        "id": "6",
        "Name": "Geoenvironmental Hazrards",
        "LearningOutcome": "Understanding geoenvironmental hazards",
        "ExitAward": "BSc",
    },
];

// Get the contentContainer div on the page and use the .innerHTML to convert what comes after 
//into html compatible text and opens a literal value to hold this
document.getElementById("contentContainer").innerHTML = `
${programs.map(function(program) { // Maps the module array one object at a time into the module pararmeter which returns the specified values
    return `
    <div id="content">
        <p>ProgramID | ${program.id}</p>
        <p>Program Name | ${program.Name}</p>
        <p>Program Learning Outcome | ${program.LearningOutcome}</p>
        <p>Program Exit Award | ${program.ExitAward}</p>
    </div>
    `
}).join('')} 
`
//Join function removes any commas, or joining characters from between array elements
